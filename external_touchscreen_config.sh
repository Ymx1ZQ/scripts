#!/bin/bash
primary_monitor=$(xrandr | grep " connected " | awk '{ print$1 }' | sed -n 1p)
external_monitor=$(xrandr | grep " connected " | awk '{ print$1 }' | sed -n 2p)
xrandr --output $external_monitor --primary
xrandr --output $external_monitor --mode 1920x1080 --pos 0x0 --rotate normal --output $primary_monitor --mode 2560x1600 --pos 1920x0 --rotate normal --primary
xinput map-to-output "Siliconworks SiW HID Touch Controller" $external_monitor